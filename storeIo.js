const fs = require('fs');



const storeDataFileName = 'storeData.json';


const loadStoreObj = () => {

    return new Promise(resolve => {

        fs.readFile(storeDataFileName, (err, data) => {
            if (err) throw err;
            const storeObj = JSON.parse(data);
            resolve(storeObj);
        });
    });
}


const saveStoreObj = (storeObj) => {

    return new Promise(resolve => {
        let data = JSON.stringify(storeObj, null, 2);

        fs.writeFile(storeDataFileName, data, (err) => {
            if (err) throw err;
            // console.log('Data written to file');
            resolve(storeObj);
        });

    });
}


module.exports = {
    loadStoreObj,
    saveStoreObj,
}
